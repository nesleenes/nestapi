import { Entity, Column, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn, BeforeInsert, getManager, OneToOne, JoinColumn, ManyToOne, OneToMany, BeforeRemove, BeforeUpdate, AfterInsert, AfterUpdate } from 'typeorm';

@Entity({ name: 'quatation' })
export class Quotations {
  constructor() {
  }
  @PrimaryGeneratedColumn({ name: 'id' }) id: number;
  @Column({ nullable: true, unique: true, name: 'name' }) name: string;
  @Column({ nullable: true, unique: true, name: 'issuedate' }) issuedate: string;
  @Column({ nullable: true, unique: true, name: 'expiredate' }) expiredate: string;
  @Column({ nullable: true, unique: true, name: 'quotationnumber' }) quotationnumber: string;
  @Column({ nullable: true, unique: true, name: 'salemanname' }) salemanname: string;
  @Column({ nullable: true, unique: true, name: 'validity' }) validity: string;
  @Column({ nullable: true, unique: true, name: 'deliverydate' }) deliverydate: string;
  @Column({ nullable: true, unique: true, name: 'note' }) note: string;
  @Column({ nullable: true, unique: true, name: 'address' }) address: string;
  @Column({ nullable: true, unique: true, name: 'codecustomer' }) codecustomer: string;
  @Column({ nullable: true, unique: true, name: 'customerName' }) customerName: string;
  @Column({ nullable: true, unique: true, name: 'customerBuilding' }) customerBuilding: string;
  @Column({ nullable: true, unique: true, name: 'customerEmail' }) customerEmail: string;
  @Column({ nullable: true, unique: true, name: 'customerTele' }) customerTele: string;
  @Column({ nullable: true, unique: true, name: 'phonenumber' }) phonenumber: string;
  @Column({ nullable: true, unique: true, name: 'credit' }) credit: string;
  @Column({ nullable: true, unique: true, name: 'discount' }) discount: string;
  @Column({ nullable: true, unique: true, name: 'district' }) district: string;
  @Column({ nullable: true, unique: true, name: 'province' }) province: string;
  @Column({ nullable: true, unique: true, name: 'amphoe' }) amphoe: string;
  @Column({ nullable: true, unique: true, name: 'zipcode' }) zipcode: string;
  @Column({ nullable: true, unique: true, name: 'type' }) type: string;
  @Column({ nullable: true, unique: true, name: 'vatType' }) vatType: string;
  @Column({ nullable: true, unique: true, name: 'vatValue' }) vatValue: string;
  @Column({ nullable: true, unique: true, name: 'total' }) total: string;
  @Column({ nullable: true, unique: true, name: 'status' }) status: string;
  @Column({ nullable: true, unique: true, name: 'discountAll' }) discountAll: string;
  @Column({ nullable: true, unique: true, name: 'netTotal' }) netTotal: string;
  @Column({ nullable: true, unique: true, name: 'idItem' }) idItem: string;
  @Column({ nullable: true, unique: true, name: 'amount' }) amount: string;
  @Column({ nullable: true, unique: true, name: 'price' }) price: string;
  @Column({ nullable: true, unique: true, name: 'discountItem' }) discountItem: string;
  @Column({ nullable: true, unique: true, name: 'sum' }) sum: string;
  
  @CreateDateColumn({ name: 'create_at', select: false }) createAt: Date;
  @UpdateDateColumn({ name: 'modify_at', update: false, select: false }) modifyAt: Date;
}