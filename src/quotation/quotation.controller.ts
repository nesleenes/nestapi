import { Controller, Get, Query, Req, Res, UseGuards ,Headers, Param, HttpException, HttpStatus, Post, UsePipes, ValidationPipe, Body, Put, Delete } from '@nestjs/common';
import { ApiBearerAuth, ApiHeaders, ApiParam } from '@nestjs/swagger';
import { User } from 'src/shared/decorator/user.decorator';
import { AuthGaurd } from 'src/shared/guard/auth.guard';
import { Quotations } from './entities/quotation.entity';
import { QuotationService } from './quotation.service';

@Controller('qu0tation')
export class QuotationController {
    constructor(
        private readonly mainServices: QuotationService,
      ) { }
    
      // Method: GET
      @ApiHeaders([{ name: 'lang', enum: ["TH", "EN", "ZH"] }])
      @Get()
      @ApiBearerAuth()
      @UseGuards(new AuthGaurd())
      async findData(@Res() res, @Req() req, @Query() query, @Headers() header) {
        const resdata = await this.mainServices.findData();
        return res.status(200).json(resdata);
      }
    
      @ApiHeaders([{ name: 'lang', enum: ["TH", "EN", "ZH"] }])
      @ApiParam({ name: 'id' })
      @Get(':id')
      @ApiBearerAuth()
      @UseGuards(new AuthGaurd())
      async findOneData(@Res() res, @Req() req, @Param() param, @Headers() header) {
        try {
          const resdata = await this.mainServices.findOneData(param.id);
          return res.status(200).json(resdata);
        } catch (error) {
          throw new HttpException(error, HttpStatus.BAD_REQUEST);
        }
      }
    
      // Method POST
      @Post()
      @ApiBearerAuth()
      @UseGuards(new AuthGaurd())
      @UsePipes(new ValidationPipe())
      async createRoom(@Res() res, @User() payload, @Req() req, @Body() body: Quotations) {
        const dataset = await this.mainServices.create(payload.id, body);
        return res.json(201).json({"message" : "save success"})
      }
    
    
      // PUT Method
      @Put(':id')
      @ApiParam({ name: 'id' })
      @UsePipes(new ValidationPipe())
      @ApiBearerAuth()
      @UseGuards(new AuthGaurd())
      updateData(@Res() res, @Body() body, @User() payload, @Req() req, @Param('id') id) {
        const dataset = this.mainServices.update(payload.id, id, body);
        return res.json(201).json(dataset)
      }
    
      // DELETE Method
      @ApiParam({ name: 'id' })
      @Delete(':id')
      @ApiBearerAuth()
      @UseGuards(new AuthGaurd())
      async deleteData(@Param('id') id, @Req() req, @Res() res, @User() payload) {
        const del = await this.mainServices.delete(id, payload.id);
        if (!del.deleted) {
          throw new HttpException('Not Found', HttpStatus.NOT_FOUND);
        }
        return res.json(204).json(null)
      }

}
