import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Quotations } from 'src/quotation/entities/quotation.entity';
import { QuotationController } from './quotation.controller';
import { QuotationService } from './quotation.service';

@Module({
  imports: [TypeOrmModule.forFeature([Quotations])],
  controllers: [QuotationController],
  providers: [QuotationService],
  exports: [QuotationService]
})
export class QuotationModule {}
