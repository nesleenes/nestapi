import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './users/user.module';
import { QuotationModule } from './quotation/quotation.module';
import { ItemModule } from './item/item.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      name: "default",
      type: "postgres",
      host: "128.199.163.90",
      port: 5432,
      username: "platong",
      password: "p8SFrPKncZqK9JR2BajsD",
      database: "qt",
      schema: "app",
      entities: [
        "dist/**/**/**/*.entity{.ts,.js}"
      ],
      synchronize: true,
      logging: true
    }),
    AuthModule,
    UserModule,
    QuotationModule,
    ItemModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
