import { Entity, Column, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn, BeforeInsert, getManager, OneToOne, JoinColumn, ManyToOne, OneToMany, BeforeRemove, BeforeUpdate, AfterInsert, AfterUpdate } from 'typeorm';

@Entity({ name: 'item' })
export class Item {
  constructor() {
  }
  @PrimaryGeneratedColumn({ name: 'id' }) id: number;
  @Column({ nullable: true, unique: true, name: 'name' }) name: string;
  @Column({ nullable: true, unique: true, name: 'idItem' }) idItem: string;
  @Column({ nullable: true, unique: true, name: 'amount' }) amount: string;
  @Column({ nullable: true, unique: true, name: 'price' }) price: string;
  @Column({ nullable: true, unique: true, name: 'discount' }) discount: string;
  @Column({ nullable: true, unique: true, name: 'sum' }) sum: string;
  @Column({ nullable: true, unique: true, name: 'idQuotation' }) idQuotation: string;
  
  @CreateDateColumn({ name: 'create_at', select: false }) createAt: Date;
  @UpdateDateColumn({ name: 'modify_at', update: false, select: false }) modifyAt: Date;
}