import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Item } from './entities/item.entity';

@Injectable()
export class ItemService {
    constructor(
        @InjectRepository(Item) private readonly mainRepositories: Repository<Item>,
      ) { }

      // Method : GET
      async findData() {
        try {
          let datas = await this.mainRepositories.find()
          return datas
        } catch (error) {
          throw new HttpException(`[findData] ${error.message}`, HttpStatus.BAD_REQUEST);
        }
      }
    
      async findOneData(id) {
        try {
          let datas = await this.mainRepositories.findOne({id})
          return datas
        } catch (error) {
          throw new HttpException(`[findData] ${error.message}`, HttpStatus.BAD_REQUEST);
        }
      }

      // Method POST
      async create(payloadId: number, data: Item){
        try {
          const maindata = await this.mainRepositories.create(data);
          await this.mainRepositories.save(maindata);
          return null;
        } catch (error) {
          throw new HttpException(error, HttpStatus.BAD_REQUEST);
        }
      }
      // PUT Menthod
      async update(payloadId: number, id: number, data: Item) {
        try {
          await this.mainRepositories.update({ id }, { ...data });
    
          const items = await this.mainRepositories.findOne({ id });
    
          return items;
        } catch (error) {
          throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
        }
      }
    
      async delete(id: number, payloadId: number) {
        try {
          await this.mainRepositories.delete({ id });
          return { deleted: true };
        } catch (error) {
          throw new HttpException(error, HttpStatus.BAD_REQUEST);
        }
      }

}
