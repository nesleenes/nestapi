import { Entity, Column, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn, BeforeInsert, getManager, OneToOne, JoinColumn, ManyToOne, OneToMany, BeforeRemove, BeforeUpdate, AfterInsert, AfterUpdate } from 'typeorm';
import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import fs = require("fs");
import CryptoJS = require('crypto-js');
import { DatetimeService } from 'src/shared/helper/datetime.service';
import { UserRO } from '../dto/user.dto';

@Entity({ name: 'users' })
export class Users extends DatetimeService {
  constructor() {
    super();
  }
  @PrimaryGeneratedColumn({ name: 'id' }) id: number;
  @Column({ nullable: true, unique: true, name: 'username' }) username: string;
  @Column({ name: 'name' , length: 50 }) name: string;
  @Column({ name: 'password' }) password: string;
  @Column({ name: 'email' }) email: string;
  @Column({ name: 'usertype' }) userType: string;
  
  @CreateDateColumn({ name: 'create_at', select: false }) createAt: Date;
  @UpdateDateColumn({ name: 'modify_at', update: false, select: false }) modifyAt: Date;

  @BeforeInsert()
  async hashPassword() {
    this.password = await bcrypt.hash(this.password.trim(), 10);
  }

  private strEncrypt(text) {
    const ciphertext = CryptoJS.AES.encrypt(JSON.stringify(text), process.env.SECRET).toString();
    return ciphertext;
  }

  private get access_token() {
    // PAYLOAD
    const { id, email, username } = this;

    const payload = this.strEncrypt({ id, email, username });

    // PRIVATE key
    const privateKEY = fs.readFileSync(`${process.env.PRIVATE_KEY}`, 'utf8');

    return jwt.sign({ payload }, privateKEY, {
      issuer: `${process.env.ISSUER}`,
      subject: `${process.env.SUBJECT}`,
      audience: `${process.env.AUDIENCE}`,
      expiresIn: "7d",
      algorithm: "RS256"
    });
  }
  async comparePassword(attempt: string) {
    return await bcrypt.compare(attempt, this.password);
  }

  async toResponseObject(showToken: boolean = true, lang = '', showPassword: boolean = false): Promise<UserRO> {
    const { id, username, password,email,name, access_token } = this;
    let responseObject: any

    responseObject = {
      id, username, password,email,name, access_token
    };

    if (showToken) {
      responseObject.access_token = access_token;
    }

    if (showPassword) {
      responseObject.password = password;
    }

    return responseObject;
  }
}