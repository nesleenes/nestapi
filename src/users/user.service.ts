import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Brackets, Raw, Like } from 'typeorm';
import * as bcrypt from 'bcryptjs';
import { Users } from './entities/user.entity';
import { UserDTO, UserRegisterDTO, UserRO } from './dto/user.dto';
import { UserAuthDTO } from './dto/user-auth.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(Users) private readonly mainRepositories: Repository<Users>,
  ) { }

  // Method : GET
  async findData() {
    try {
      let datas = await this.mainRepositories.find()
      return datas
    } catch (error) {
      throw new HttpException(`[findData] ${error.message}`, HttpStatus.BAD_REQUEST);
    }
  }

  async findOneData(id) {
    try {
      let datas = await this.mainRepositories.findOne({id})
      return datas
    } catch (error) {
      throw new HttpException(`[findData] ${error.message}`, HttpStatus.BAD_REQUEST);
    }
  }

  async userValidate(data: UserAuthDTO, lang: string = ''): Promise<UserRO> {
    try {
      const { username, password } = data;

      const users = await this.mainRepositories.findOne({
        where: { username }
      })

      if (!users || (!await users.comparePassword(password))) {
        throw new HttpException("Invalid username/password.", HttpStatus.BAD_REQUEST);
      }
      return await users.toResponseObject(true, lang, true);
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }




  // Method POST
  async create(payloadId: number, data: Users){
    try {
      const maindata = await this.mainRepositories.create(data);
      await this.mainRepositories.save(maindata);

      return maindata.toResponseObject();
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  // Method POST
  async register(data: UserRegisterDTO) {
    try {
      const items = await this.mainRepositories.create(data);
      await this.mainRepositories.save(items);
      return items.toResponseObject(false);
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }




  // PUT Menthod
  async update(payloadId: number, id: number, data: UserDTO): Promise<UserRO> {
    try {
      const { password } = data;
      delete data.password;

      if (password) {
        Logger.log(password, "hashpass")
        const hashpass = await bcrypt.hash(password, 10);
        Object.assign(data, { password: hashpass });
      }

      await this.mainRepositories.update({ id }, { ...data });

      const items = await this.mainRepositories.findOne({ id });

      return items.toResponseObject(false, "", false);
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
  }

  async delete(id: number, payloadId: number) {
    try {
      await this.mainRepositories.delete({ id });
      return { deleted: true };
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
}
