import { Controller, Post, Req, Res, Body, HttpException, HttpStatus, Headers, UsePipes, ValidationPipe, Logger } from '@nestjs/common';
import { ApiHeaders, ApiTags } from '@nestjs/swagger';
import { UserAuthDTO } from 'src/users/dto/user-auth.dto';
import { Users } from 'src/users/entities/user.entity';
import { UserService } from 'src/users/user.service';
import { AuthService } from './auth.service';

@ApiTags('Authentication & Access')
@Controller('auth')
export class AuthController {
  constructor(
    private readonly userService: UserService,
    private readonly mainServices: AuthService,
    
  ) { }

  @ApiHeaders([{ name: 'lang', enum: ["TH", "EN", "ZH"] }])
  @Post('verify')
  @UsePipes(new ValidationPipe())
  async login(@Req() req, @Res() res, @Body() body: UserAuthDTO, @Headers() header) {
    const lang = (typeof header.lang !== 'undefined' ? header.lang : '').toUpperCase();
    const items = await this.userService.userValidate(body, (lang == 'ZH' ? 'CN' : lang))
    return res.status(201).json(items);
  }

  @Post('register')
  @UsePipes(new ValidationPipe())
  async register(@Body() data: Users) {
    return this.mainServices.register(data);
  }
}
